const express = require('express');
const bodyParser = require('body-parser');
const api = require('./api/api.js');

const server = express();
const root = './build';

server.use(express.static(root));
server.use(bodyParser.json());

server.get('/weather/:location', (request, response) => {
   response.sendFile('/', { root });
});

server.post('/api/get/', (request, response) => {
   api.getLocation(request.body)
      .then(location => api.getWeather(location))
      .then(weather => response.json({ weather }))
      .catch(error => response.json({ error }));
});

server.listen(5000, () => {
   console.log('Arcus Weather App is listening on port 5000');
});
