import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import './App.css';
import Weather from './Weather.js';
import Description from './Description.js';
import logo from './images/logo.svg';

export default class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
         location: ''
		};
		this.onInputChange = this.onInputChange.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.onButtonClick = this.onButtonClick.bind(this);
	}

	onInputChange(event) {
		const location = event.target.value;
      this.setState({ location });		
	}

	onFormSubmit(event) {
      event.preventDefault();
	}

	onButtonClick() {
		this.setState({ location: '' });
	}

	render() {
		let { location } = this.state;
		return (
			<Router>
				<div className="App">
					<header>
						<Link to="/"><img src={logo} alt="logo" /></Link>
						<h1>Arcules Weather App with React</h1>
					</header>
					<section>
						<form onSubmit={this.onFormSubmit}>
                     <input name="location" placeholder="Location" value={location} onChange={this.onInputChange} />               
						</form>
						<Link to={`/weather/${location}`} onClick={this.onButtonClick}>Get weather</Link>
						<Route exact path="/" component={Description} />
						<Route exact path="/weather/:location" component={Weather} />
					</section>
				</div>
			</Router>
		);
	}
}