import React from 'react';

import './Description.css';

export default function Description() {
   return (
      <div className="Description">
         <h1>Description</h1>
         <p>
            Please, select a location to get the weather information for the past 7 days.
         </p>
         <i>e.g. Los Angeles, London, Sofia, etc.</i>
      </div>
   );
}