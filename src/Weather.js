import React, { Component } from 'react';

import './Weather.css';

export default class Weather extends Component {
	constructor(props) {
		super(props);
		
      let { match: { params: { location }}} = props;
      this.state = {
         location,
         days: []
      };
      this.get = this.get.bind(this);
      this.getCallback = this.getCallback.bind(this);

      this.get();
   }
   
   componentWillReceiveProps(props) {
      let { match: { params: { location }}} = props;

      if (location !== this.state.location) {
         this.setState({ location }, this.get);
      }
   }

   get() {
      let { location } = this.state;
      const timestamp = Math.floor(new Date().getTime() / 1000);

      const parameters = {
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json'
         },
         method: "POST",
         body: JSON.stringify({ location, timestamp })
      }

      fetch('/api/get/', parameters)
         .then(response => response.json())
         .then(data => this.getCallback(data))
         .catch(error => console.log(error));
   }

   getCallback(data) {
      console.log(data);
      if (data.error) return;

      const days = data.weather.map(day => {
         const timezone = day.response.timezone;
         const offset = day.response.offset * 60 * 60;
         const data = day.response.daily.data[0];
         return {
            timezone: timezone,
            timestamp: data.time + offset,
            summary: data.summary,
            temperature: {
               max: Math.round(data.temperatureMax),
               min: Math.round(data.temperatureMin)
            }
         };
      });
      this.setState({ days });
   }

   getDate(timestamp) {
      const date = new Date(timestamp * 1000);
      const day = ('0' + date.getDate()).slice(-2);
      const month = ('0' + (date.getMonth() + 1)).slice(-2);
      const year = date.getFullYear();

      return `${day}.${month}.${year}`;
   }

   getDay(timestamp) {
      const day = new Date(timestamp * 1000).getDay();
      return weekDays[day];
   }

	render() {
      let { location } = this.state;
      let { timezone } = this.state.days[0] || '';
      
      const days = this.state.days.map(day => {
         return (
            <li key={day.timestamp}>
               <h4>{this.getDay(day.timestamp)}</h4>
               <h5>
                  <b>{day.temperature.max}°C</b>
                  <i>{day.temperature.min}°C</i>
               </h5>
               <p>{this.getDate(day.timestamp)}</p>
               <p>{day.summary}</p>
            </li>
         );
      });

		return (
			<div className="Weather">
				<dl>
               <dt>Location:</dt><dd>{location}</dd>
               <dt>Timezone:</dt><dd>{timezone}</dd>
            </dl>
				<ul>{days}</ul>
			</div>
		);
	}
}

const weekDays = [
   'Sunday',
   'Monday',
   'Tuesday',
   'Wednesday',
   'Thursday',
   'Friday',
   'Saturday'
];