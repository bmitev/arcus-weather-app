PROJECT_ID=arcus-weather-app
IMAGE=gcr.io/$PROJECT_ID/arcus-weather-app:$(git log --format="%h" -n 1)
HOST_PORT=8080
CONTAINER=arcus-weather-app-container
CONTAINER_PORT=5000
CLUSTER=arcus-weather-app-cluster
DEPLOYMENT=arcus-weather-app-deployment

git pull
docker pull node:carbon
docker build -t $IMAGE .

docker stop $CONTAINER > /dev/null
docker rm $CONTAINER > /dev/null
docker run --name=$CONTAINER --restart=always -p $HOST_PORT:$CONTAINER_PORT -d $IMAGE
docker image prune --force

google-cloud-sdk/bin/gcloud docker -- push $IMAGE
google-cloud-sdk/bin/gcloud container clusters get-credentials $CLUSTER --project $PROJECT_ID

# Production init

# google-cloud-sdk/bin/kubectl run $DEPLOYMENT --image=$IMAGE --port $CONTAINER_PORT
# google-cloud-sdk/bin/kubectl expose deployment $DEPLOYMENT --type=LoadBalancer --port $HOST_PORT --target-port $CONTAINER_PORT

# Production update

google-cloud-sdk/bin/kubectl set image deployment/$DEPLOYMENT $DEPLOYMENT=$IMAGE