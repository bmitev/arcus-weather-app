const request = require('request');

const apiKeyDarksky = '8d15bebf8d1003e410ff3540494c19b3';
const apiKeyGoogle = 'AIzaSyB8GgPWZDna-Uf9ARIK5RZ5A2ytWfmqHuE';

module.exports = new function API() {
   this.getLocation = getLocation;
   this.getWeather = getWeather;
}

function getLocation(parameters) {
   let { location, timestamp } = parameters;
   const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(location)}&key=${apiKeyGoogle}`;

   return new Promise((resolve, reject) => {
      request(url, (error, response, body) => {
         const data = body && JSON.parse(body);
         error = data && data.error_message || error;
         if (error) {
            reject(error);
         }
         else {
            console.log(data);
            let { lat: latitude, lng: longitude } = data.results[0].geometry.location;
            resolve({ latitude, longitude, timestamp });
         }
      });
   });
}

function getWeather(parameters) {
   let { latitude, longitude, timestamp } = parameters;

   latitude = Math.min(Math.max(parseFloat(latitude), -90), 90);
   longitude = Math.min(Math.max(parseFloat(longitude), -180), 180);

   if (isNaN(latitude) || isNaN(longitude)) {
      return Promise.reject({ message: "Latitude or longitude not a number." });      
   }
   const timestamps = getTimestamps(timestamp);
   const promises = timestamps.map(timestamp => {
      const url = `https://api.darksky.net/forecast/${apiKeyDarksky}/${latitude},${longitude},${timestamp}?exclude=currently,minutely,hourly,alerts,flags&units=si`;

      const promise = new Promise((resolve, reject) => {
         request(url, (error, response, body) => {
            const data = body && JSON.parse(body);
            error = data && data.error || error;
            if (error) {
               reject(error);
            }
            else {
               resolve({ latitude, longitude, timestamp, response: data });
            }
         });
      });
      return promise;
   });
   return Promise.all(promises);
}

function getTimestamps(timestamp) {
   const offset = 24 * 60 * 60;

   let timestamps = [];

   for (var i = -7; i < 0; i++) {
      timestamps.push(timestamp + i * offset);
   }
   return timestamps;
}